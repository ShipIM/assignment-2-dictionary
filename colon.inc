%define colon_pointer 0

%macro colon 2
    %%key:
        db %1, 0

    %%colon_start:
        dq %%key
        dq %2
        dq colon_pointer

    %2:

    %define colon_pointer %%colon_start
%endmacro
%include "lib.inc"
%include "dict.inc"
%include "colon.inc"
%include "words.inc"

%define buffer_size 255
%define SHIFT_TO_VALUE 8
%define ERROR_EXIT_CODE 1
%define STD_ERR 2
%define STD_OUT 1

section .data
buffer_overflow:
    db "The entered string exceeds the buffer size", 0
    
no_such_key:
    db "There is no such key in the dictionary", 0

section .bss
buffer_address: resb buffer_size

section .text
global _start

_start:
    mov rdi, buffer_address
    mov rsi, buffer_size

    call read_line
    test rax, rax
    je .buffer_overflow_error

    mov rdi, rax
    mov rsi, colon_pointer
    call find_word

    test rax, rax
    jne .print_success

    .no_such_key_error:
        mov rdi, no_such_key

        jmp .print_error

    .buffer_overflow_error:
        mov rdi, buffer_overflow

    .print_error:
        mov rsi, STD_ERR

        call print_string

        mov rdi, ERROR_EXIT_CODE
        jmp .end

    .print_success:
        mov rdi, [rax + SHIFT_TO_VALUE]

        mov rsi, STD_OUT

        call print_string
        xor rdi, rdi

    .end:
        call exit
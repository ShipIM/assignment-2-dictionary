ASM = nasm
ASMFLAGS = -g -felf64
LD = ld

main: main.o lib.o dict.o
	$(LD) -o $@ $^

main.o: main.asm dict.inc lib.inc colon.inc words.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

dict.o: dict.asm lib.inc

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

clean:
	$(RM) *.o
	$(RM) main

.PHONY: clean
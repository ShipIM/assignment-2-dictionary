%include "lib.inc"  

%define SHIFT_TO_THE_NEXT_ELEMENT 16
                                
global find_word                
                                
find_word:                      
    test rsi, rsi                
    je .no_such_key             
                                
    push rdi                    
    push rsi                    
                                
    mov rsi, [rsi]              
                                
    call string_equals           
                                
    pop rsi                     
    pop rdi                     
                                
    cmp rax, 1                  
    je .success                 
                                
    .try_next_time:             
        mov rsi, [rsi + SHIFT_TO_THE_NEXT_ELEMENT]
                                
        jmp find_word                            
                                
    .no_such_key:                
        xor rax, rax              
        jmp .end                
                                
    .success:                   
        mov rax, rsi            
                                
    .end:                       
        ret
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global read_line

section .text
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60

    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rcx, rcx
    
    .loop:
        movzx rax, byte[rdi + rcx]
        cmp rax, 0
        je .end
        inc rcx
        jmp .loop

    .end:
        mov rax, rcx
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rdx, rax
    mov rax, 1
    push rsi
    mov rsi, rdi
    pop rdi

    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, '\n'

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdx, 1
    mov rsi, rsp
    mov rdi, 1

    syscall
    pop rdi
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jl .negative
    jmp print_uint

    .negative:
        push rdi
        mov rdi, '-'

        call print_char

        pop rdi
        neg rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r9, rsp
    mov rsi, 10

    dec rsp
    mov byte[rsp], 0
    
    .divide:
        xor rdx, rdx
        div rsi
        add dl, '0'
        dec rsp
        mov [rsp], dl

        test rax, rax 
        je .end       
        jmp .divide

    .end:
        mov rdi, rsp
        push r9

        call print_string

        pop r9
        mov rsp, r9
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor r8, r8

    .loop:
        mov al, [rdi + r8]
        cmp al, byte[rsi + r8]
        jne .false

        cmp al, 0
        je .true

        inc r8
        jmp .loop
    
    .false:
        xor rax, rax
        jmp .end
    
    .true:
        mov rax, 1

    .end:
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0

    .read:
        xor rax, rax
        xor rdi, rdi
        mov rsi, rsp
        mov rdx, 1

        syscall

    .end:
        pop rax
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rcx, rcx
  
    .read:
        push rdi
        push rsi
        push rcx

        call read_char

        pop rcx 
        pop rsi
        pop rdi

        cmp rax, 0
        je .success

        cmp rax, ' '
        je .spaces
        
        cmp rax, `\t`
        je .spaces

        cmp rax, `\n`
        je .spaces

        mov [rdi + rcx], rax
        inc rcx

        cmp rcx, rsi
        jge .error

        jmp .read

    .spaces:
        cmp rcx, 0
        je .read
    
    .success:
        xor rax, rax
        mov [rdi + rcx], rax

        mov rax, rdi
        mov rdx, rcx

        jmp .end

    .error:
        mov rax, 0

    .end:
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер строку из stdin
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
read_line:
    xor rcx, rcx
    push r12
    push r13
    push r14

    mov r12, rdi
    mov r13, rsi
    mov r14, rcx
  
    .read:
        call read_char

        test rax, rax
        je .success
        cmp rax, `\n`
        je .success

        mov [r12 + r14], rax
        inc r14

        cmp r14, r13
        jge .error

        jmp .read
    
    .success:
        xor rax, rax
        mov [r12 + r14], rax

        mov rax, r12
        mov rdx, r14

        jmp .end

    .error:
        xor rax, rax

    .end:
        pop r14
        pop r13
        pop r12
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov rsi, 10
    
    .read:
        movzx r8, byte[rdi + rcx]

        cmp r8b, 0
        je .end
        
        cmp r8b, '0'
        jl .end            

        cmp r8b, '9'
        jg .end
        
        sub r8b, '0'
        mul rsi
        add rax, r8 

        inc rcx
        
        jmp .read

    .end:
        mov rdx, rcx
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov al, byte[rdi]
    cmp al, '-'
    je .negative

    jmp parse_uint

    .negative:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
    
    .end:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx
    xor rax, rax

    .copy:
        cmp byte[rdi + rcx], 0
        je .success

        cmp rcx, rdx
        jge .error
        
        mov al, byte[rdi + rcx]
        mov [rsi + rcx], al
        inc rcx

        jmp .copy

    .error:
        mov rax, 0
        jmp .end
        
    .success:
        xor rdi, rdi
        mov [rsi + rcx], rdi

        mov rax, rcx

    .end:
        ret